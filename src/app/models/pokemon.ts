export interface Pokemon {
    sprite: string;
    types?: any[];
    base_stats?: any[];
    name: string;
    height?: number;
    weight?: number;
    abilities?: any[];
    baseExperience?: number;
    moves?: any[];
    url: string;
}