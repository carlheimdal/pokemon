import { Component, OnInit } from '@angular/core';
import { PokemonApiService } from '../services/pokemon-api.service';
import { Pokemon } from '../models/pokemon';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit {

    pokemonDetail: Pokemon = null;
    private pokemonDetail$: Subscription;
    private name: string;

  constructor(private pokemonApiService: PokemonApiService,
              private route: ActivatedRoute) {

    this.name = this.route.snapshot.paramMap.get('name');

    this.pokemonDetail$ = this.pokemonApiService.pokemonDetail$.subscribe(
        pokemonDetail => {
            this.pokemonDetail = pokemonDetail;
            console.log('pokemondetail: ', this.pokemonDetail);
        }
    )
   }

   get stats(){
       return this.pokemonDetail.base_stats;
   }

   get types(){
       return this.pokemonDetail.types;
   }

   get moves(){
       return this.pokemonDetail.moves;
   }

  ngOnInit(): void {
    console.log('name: ', this.name);
    this.pokemonApiService.getDetails(this.name);
  }

}
