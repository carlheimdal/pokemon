import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/authservice';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
@Injectable({
  providedIn: 'root'
})
export class HomeComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { }

  trainerName: string;

    get isLoggedIn(){
        return this.authService.isLoggedIn();
    }

    get name(){
      return localStorage.getItem('trainerName');
    }

  ngOnInit(): void {}

  storeName(){
    localStorage.setItem('trainerName', this.trainerName);
    this.router.navigate(['trainer']);
  }

}
