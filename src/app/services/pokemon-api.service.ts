import { map, catchError, first, } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { Pokemon } from '../models/pokemon';
import { environment as env } from '../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PokemonApiService {
  
  public pokemon$: BehaviorSubject<Pokemon[]> = new BehaviorSubject([]);
  public pokemonDetail$: BehaviorSubject<Pokemon> = new BehaviorSubject(null);
  public trainerCollection$: BehaviorSubject<Pokemon> = new BehaviorSubject(null);

  private limit: string = '/pokemon?limit=20';
  private offset: string = '&offset=0';

  readonly IMG_BASEURL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/";

  constructor(private http: HttpClient, private router: Router) {}

  getPokemons(): void{
	this.http.get<Pokemon[]>(env.apiBaseUrl + this.limit + this.offset)
	.pipe(
	  map((res:any)=> res.results.map( p =>{
		return{
		  url: p.url,
		  name: p.name,
		  sprite: this.IMG_BASEURL + p.url.split('/').filter(Boolean).pop() + '.png',
		  id: p.url.split('/').filter(Boolean).pop()
		}
	  }))
	)
	.subscribe(pokemons => {
	  this.pokemon$.next(pokemons);
	});
  }

  getDetails(name: string){
	  let currentUrl = env.apiBaseUrl + '/pokemon/' + name;
	this.http.get<Pokemon>(currentUrl)
	.pipe(
		map((res:any) =>{
			return{
			sprite: this.IMG_BASEURL + res.id + '.png',
			types: res.types,
			base_stats: res.stats,
			name: res.name,
			height: res.height,
			weight: res.weight,
			abilities: res.abilities,
			baseExperience: res.base_experience,
			moves: res.moves,
			url: currentUrl
			}
		})
	).subscribe(pokemonDetail => {
		this.pokemonDetail$.next(pokemonDetail);
		this.trainerCollection$.next(pokemonDetail);
	});
  }

}
