export class AuthService {

    isLoggedIn(): boolean {
        let name = localStorage.getItem('trainerName');
        if(name == null || name === '') {
            console.log('returning false');
            return false;
        } else {
            return true;
        }
    }
    
}