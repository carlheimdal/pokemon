import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnlyLoggedInUsersGuard} from './guards/auth/auth.guard';
import { HomeComponent } from './home/home.component';
import { TrainerComponent } from './trainer/trainer.component';
import { PokemonCatalogueComponent} from './pokemon-catalogue/pokemon-catalogue.component'
import { AuthService } from './services/authservice';
import { PokemonDetailComponent } from './pokemon-detail/pokemon-detail.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'trainer', component: TrainerComponent},
  {
    path: 'catalogue',
    component: PokemonCatalogueComponent,
    canActivate: [OnlyLoggedInUsersGuard],
  },
  {
    path: 'pokemon/:name',
    component: PokemonDetailComponent,
    canActivate: [OnlyLoggedInUsersGuard],
  }
]

@NgModule({
  providers: [AuthService, OnlyLoggedInUsersGuard],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
