import { Component, OnInit } from '@angular/core';
import { PokemonApiService } from '../services/pokemon-api.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Pokemon } from '../models/pokemon';

@Component({
  selector: 'app-list',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss']
})
export class TrainerComponent implements OnInit {

  name: string = '';
  pokemons: string[];
  pokemonDetail: Pokemon = null;
  public trainerCollection: Pokemon[] = [];
  private trainerCollection$: Subscription;

  constructor(private pokemonApiService: PokemonApiService, private router: Router) {
    this.trainerCollection$ = this.pokemonApiService.trainerCollection$.subscribe(pokemon => {
      this.pokemonDetail = pokemon;
      if(pokemon){this.trainerCollection.push(pokemon);}
    })
  }

  ngOnInit(): void {
    this.name = localStorage.getItem('trainerName');
    this.pokemons = JSON.parse(localStorage.getItem('pokemons'));
    if(this.pokemons !== null){this.pokemons.forEach((pokemon) => {
        this.pokemonApiService.getDetails(pokemon);
        console.log('getting: ', pokemon);
      })
    }
    console.log('pokemons: ', this.pokemons);
    console.log('trainerCollection: ', this.trainerCollection);
  }

  ngOnDestroy(): void {
    this.trainerCollection$.unsubscribe();
  }

  onRemoveClicked(name: string): void{
    this.trainerCollection = this.trainerCollection.filter(pokemon => {
      pokemon.name != name;
      });
    this.pokemons = this.pokemons.filter(pokemon => pokemon != name);
    localStorage.setItem('pokemons', JSON.stringify(this.pokemons));
    console.log(this.trainerCollection);
    console.log(this.pokemons);
  }

}
