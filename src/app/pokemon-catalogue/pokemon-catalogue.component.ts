import { Component, OnInit, Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { PokemonApiService } from '../services/pokemon-api.service';
import { Pokemon } from '../models/pokemon';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.scss']
})

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueComponent implements OnInit, OnDestroy {

  public pokemons: Pokemon[] = [];
  private pokemon$: Subscription;

  constructor(private pokemonApiService: PokemonApiService,
              private router: Router) {
    this.pokemon$ = this.pokemonApiService.pokemon$.subscribe(pokemon => {
      this.pokemons = pokemon;
    })
  }

  onPokemonClicked(name: string){
    this.router.navigateByUrl('pokemon/' + name);
  }

  // Store collected pokemon id's in localstorage
  onCollectClicked(name: string): void{
    console.log('id: ', name);
    let pokemons = JSON.parse(localStorage.getItem('pokemons')) || [];
    pokemons.push(name);
    localStorage.setItem('pokemons', JSON.stringify(pokemons));
  }

  ngOnInit(): void {
    this.pokemonApiService.getPokemons();
  }

  ngOnDestroy(): void {
    this.pokemon$.unsubscribe();
  }
}
